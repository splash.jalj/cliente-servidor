package hilo;
public class Hilo extends Thread{
    String id;
    
    public Hilo(String id){
         this.id=id;
         System.out.println("Objeto creado!"+ id + "!");
    }
    public void run(){
    for(int i=0; i<100; i++){
        System.out.println("Hilo "+id+"   valor de i="+i);
    }
    }

    public static void main(String[] args) {
        new Hilo("Alfa").start();
        new Hilo("...BETA").start();
    }
    
}

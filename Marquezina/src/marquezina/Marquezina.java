/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marquezina;
import javax.swing.*;
import java.awt.*;

public class Marquezina extends JFrame implements Runnable{
       private JLabel lblEtiqueta1;
       private JLabel lblEtiqueta2;
       
       public Marquezina(String msg1, String msg2){
       setLayout(new BorderLayout());
       lblEtiqueta1 = new JLabel(msg1);
       lblEtiqueta1.setFont(new Font("Papyrus",Font.BOLD,30));
       lblEtiqueta1.setForeground(Color.red);
       lblEtiqueta2 = new JLabel(msg2);
       lblEtiqueta2.setFont(new Font("Comic Snas MS",Font.BOLD,30));
       lblEtiqueta2.setForeground(Color.BLUE);
       add(lblEtiqueta1,BorderLayout.NORTH);
       add(lblEtiqueta2,BorderLayout.SOUTH);
       setTitle("Marquezina");
       setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       setResizable(false);
       setSize(400,300);
       setLocationRelativeTo(null);
       setVisible(true);
    }
    public void run(){
       StringBuffer buffer = new StringBuffer();
       char letras[]=lblEtiqueta1.getText().toCharArray();
       while (true){
            for (int i=0; i<letras.length; i++){ //Agrega letra por letra
                buffer.append(letras[i]);
                lblEtiqueta1.setText(buffer.toString());
                try{
                    Thread.currentThread().sleep(50);
                }catch(Exception error){
                }
            }
            for (int i=0; i<letras.length; i++){ //Elimina letra por letra del Buffer
                buffer.deleteCharAt(0);
                lblEtiqueta1.setText(buffer.toString());
                try{
                    Thread.currentThread().sleep(50);
                }catch(Exception error){
                }
            }
       }
    }
    
    public static void main(String args[]){
        new Thread(new Marquezina("1 GUN´S AND ROSES", "AEROSMITH")).start();
        new Thread(new Marquezina("1 GUN´S AND ROSES", "AEROSMITH")).start();
        new Thread(new Marquezina("2 ROSES", "SMITH")).start();
        new Thread(new Marquezina("2 ROSES", "SMITH")).start();
    //se crea el hilo y se envia
    }
    
}
import java.net.Socket;
import java.net.ConnectException;
public class Cliente {
    private final int PUERTO=9998;
    private final String IP_SERVIDOR= "127.0.0.1";
    private Socket miCliente;
    
    public Cliente(){
        try{
            miCliente= new Socket(IP_SERVIDOR,PUERTO);
            System.out.println("Un servidor encontrado en el puerto: " +PUERTO);
        }
        catch(ConnectException error){
        System.out.println("Servidor" +IP_SERVIDOR+"No encontrado");
        }
        catch (Exception error){
            System.out.println(error);
        }
    }
    public static void main(String args[]){
        new Cliente();
    }
}

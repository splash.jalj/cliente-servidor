package hilos;
public class Hilo1 implements Runnable {
    String id;
    public Hilo1(String id){
    this.id=id;
    System.out.println("Objeto creado!"+id+"!");
    }
    
    public void run(){
      for (int i=0; i<100; i++){
          System.out.println("Hilo "+id+"    valor de i="+i);
      }
    }
    public static void main(String args[]){
           Hilo1 alfa=new Hilo1("Alfa");
           Hilo1 beta=new Hilo1("...BETA");
           System.out.println("Aun no se inician los hilos");
           new Thread(alfa).start(); //se crea un hilo y se envia
           new Thread(beta).start(); //se crea un hilo y se envia
    }
    
}

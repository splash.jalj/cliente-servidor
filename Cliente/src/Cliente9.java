/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.net.Socket;
import java.net.ConnectException;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.DataInputStream;
import java.io.IOException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Cliente9 extends JFrame implements Runnable {
        private final int PUERTO = 9999;
        private final String IP_SERVIDOR = "127.0.0.1";
        private Socket miCliente;
        private DataOutputStream enviaDatos;
        private DataInputStream recibeDatos;
        private JTextArea txtVista;
        private JTextField txtMensaje;
        private JList lstUsuarios;
        private JSplitPane splitVertical;
        
        public Cliente9(){
              try{
                  miCliente = new Socket(IP_SERVIDOR, PUERTO);
                  System.out.println("Puerto utilizado:" + miCliente.getLocalPort());
                  System.out.println("Servidor encontrado");
                  enviaDatos = new DataOutputStream(miCliente.getOutputStream());
                  recibeDatos = new DataInputStream(miCliente.getInputStream());
                  enviar("{HELLO}," + solicitaUserName());
                  crearUI();
                  cerrar();
              }catch(ConnectException error){
                  System.out.println("Servidor " + IP_SERVIDOR + "   no encontrado!");
              }catch(Exception error){
                  System.out.println(error);
              }
        }
        private String solicitaUserName(){
            String username = JOptionPane.showInputDialog(null,
                                                          "Escribe tu UserName",
                                                          "UserName",
                                                          JOptionPane.QUESTION_MESSAGE);
            if(username.equals("")){
                JOptionPane.showMessageDialog(null,
                                              "Debe de escribir su UserName",
                                              "ERROR",
                                              JOptionPane.ERROR_MESSAGE);
                System.exit(0);
            }
            return username;
        }
        private void crearUI(){
           setLayout(new BorderLayout());
           txtVista = new JTextArea();
           txtVista.setEditable(false);
           txtMensaje = new JTextField();
           txtMensaje.addActionListener(new ActionListener(){
           public void actionPerformed(ActionEvent evt){
               enviar(txtMensaje.getText());
               txtMensaje.setText("");
           }  
        });
        
        JScrollPane scrollVista = new JScrollPane(txtVista);
            lstUsuarios  =  new JList();
            JScrollPane  scrollUsuarios = new JScrollPane(lstUsuarios);
            splitVertical = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                                           scrollVista,
                                           scrollUsuarios);
            splitVertical.setOneTouchExpandable(true);
            splitVertical.setDividerLocation(300);
            addWindowListener(new WindowAdapter(){
                public void windowClosing(WindowEvent evt){
                   enviar("{BYE}");
                }
            });
            add(splitVertical,BorderLayout.CENTER);
            add(txtMensaje,BorderLayout.SOUTH);
            setTitle("IstmoChat");
            setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            setResizable(false);
            setSize(400,300);
            setLocationRelativeTo(null);
            setVisible(true);
        }
        public void cerrar(){
            try{
                this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                addWindowListener(new WindowAdapter(){
                   public void windowClosing(WindowEvent e){
                    confirmarSalida();
                   }
                });
                this.setVisible(true);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        public void confirmarSalida(){
            int valor = JOptionPane.showConfirmDialog(null, "¿Estas seguro?", "Alerta!", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (valor==JOptionPane.YES_OPTION){
                System.exit(0);
            }
        }
        private String recibir(){
            String mensaje = null;
               try{
                  mensaje = recibeDatos.readUTF();
               }catch(IOException error){
                  System.out.println("Error al enviar mensaje!");
               }
               return mensaje;
               }
        private void enviar(String mensaje){
               try{
                  enviaDatos.writeUTF(mensaje);
                  enviaDatos.flush();
               }catch(IOException error){
                System.out.println("Error al enviar mensaje!");
               }
        }
        public void run(){
            String mensaje = "";
            while(true){
                mensaje = recibir();
                if(mensaje.contains("{LIST}")){
                   String texto = mensaje.substring(0,mensaje.indexOf("{LIST}"));
                   String usuarios  = mensaje.substring(mensaje.indexOf("{LIST}") + "{LIST}".length());
                   lstUsuarios.setListData(usuarios.split(","));
                   mensaje = texto;
                }
                txtVista.append(mensaje + "\n");
            }
        }
        
   public static void main(String args[]){
       new Thread(new Cliente9()).start();
   }
} 





import java.net.Socket;
import java.net.ConnectException;
import java.io.DataInputStream;
import java.io.IOException;

public class Cliente5 {
    private final int PUERTO=9999;
    private final String IP_SERVIDOR = "127.0.0.1";
    private  Socket miCliente;
    private DataInputStream recibeDatos;
    public Cliente5(){
        try{
            miCliente = new Socket(IP_SERVIDOR,PUERTO);
            System.out.println("Puerto utilizado:"+miCliente.getLocalPort());
            System.out.println("servidor encontrado");
            recibeDatos = new DataInputStream(miCliente.getInputStream());//recibe el mensaje enviado del servidor
            System.out.println("desde el cliente servidor dijo:"+recibir());
        }catch(ConnectException error){
            System.out.println("servidor"+IP_SERVIDOR+"NO ENCONTRADO");
            
        }catch(IOException error){
            System.out.println(error);
        }
    }
private String recibir(){
    String mensaje = null;
    try{
        mensaje = recibeDatos.readUTF();
        
    }catch(IOException error){
        System.out.println("Error al enviar mensaje");
        
    }
    return mensaje;
}
    
    public static void main(String[] args) {
        new Cliente5();
        
    }
    
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;
import java.net.Socket;
import java.net.ConnectException;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.DataInputStream;
import java.io.IOException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Cliente8 extends JFrame implements Runnable{
       private final int PUERTO = 9999;
       private final String IP_SERVIDOR = "127.0.0.1";
       private Socket miCliente;
       private DataOutputStream enviaDatos;
       private DataInputStream recibeDatos;
       private JTextArea txtVista;
       private JTextField txtMensaje;
       
       public Cliente8(){
               try{
                  miCliente = new Socket(IP_SERVIDOR, PUERTO);
                  System.out.println("Puerto utilizado:" + miCliente.getLocalPort());
                  System.out.println("Servidor Encontrado");
                  enviaDatos = new DataOutputStream(miCliente.getOutputStream());
                  recibeDatos = new DataInputStream(miCliente.getInputStream());
                  crearUI();
               }catch(ConnectException error){
                   System.out.println(error);
               }catch(Exception error){
                   System.out.println(error);
               }   
       }
       private void crearUI(){
               setLayout(new BorderLayout());
               txtVista = new JTextArea();
               txtVista.setEditable(false);
               txtMensaje = new JTextField();
               txtMensaje.addActionListener(new ActionListener(){  //EsteEscuchadorEntraEnFuncionCuandoLeDamosEnterAlaCajaDeTexto,EnCasoDeQueElMensajeSe
                    public void actionPerformed(ActionEvent evt){  //aBYEfinalizaLaComunicacion
                          enviar(txtMensaje.getText());
                          if(txtMensaje.getText().equals( "BYE" )){
                             txtMensaje.setEditable(false);
                          }
                          txtMensaje.setText("");
                    }
               });
               add(txtVista,BorderLayout.CENTER);
               add(txtMensaje,BorderLayout.SOUTH);
               setTitle("Sala de Chat  "+ miCliente.getInetAddress().getHostAddress()+ "  puerto:   "+miCliente.getLocalPort()); 
               setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
               setResizable(false);
               setSize(400,300);
               setLocationRelativeTo(null);
               setVisible(true);
       }
       private String recibir(){
               String mensaje = null;
               try{
                   mensaje = recibeDatos.readUTF();
               }catch(IOException error){
                   System.out.println("Error al enviar mensaje!");
               }
               return mensaje;

       }
       private void enviar(String mensaje){
               try{
                   enviaDatos.writeUTF(mensaje);
                   enviaDatos.flush();
                }catch(IOException error){
                   System.out.println("Error al enviar mensaje");
                }
        }
        public void run(){  //EsteProcesoEntraEnCicloInfinitoYagregaraAlAreaDeTextoTodosLosMensajesQueRecibaDelServidor
                while(true){
                    txtVista.append(recibir() + "\n");
                }
        }
        public static void main(String args[]){
                new Thread(new Cliente8()).start();        //seCreaElHiloYseEnvia
        }
    
}

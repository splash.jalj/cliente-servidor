package servidor;
import java.net.ServerSocket;
public class Servidor2 {
    private final int PUERTO=9999;
    private ServerSocket miServidor;
    
    public Servidor2(){
        try{
            miServidor = new ServerSocket(PUERTO);
            while(true){
                System.out.println("Servidor activo en el puerto: "+PUERTO);
                System.out.println("Esperando clientes...");
                miServidor.accept();
                System.out.println("Un cliente se conectó");
            }
            }catch (Exception error){
                System.out.println(error);}
        }
    public static void main(String args[]){
            new Servidor2();
    }
}

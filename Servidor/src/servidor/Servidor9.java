/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.InetAddress;
import java.util.ArrayList;

public class Servidor9 {
    private final int PUERTO = 9999;
    private ServerSocket miServidor;
    private Socket miCliente;
    
    public static ArrayList<ServidorHilo9> misClientes;
    
    public Servidor9(){
        misClientes = new ArrayList<ServidorHilo9>();
        try{
          miServidor = new ServerSocket(PUERTO);
          while(true){
              System.out.println("Servidor Activo, PUERTO: " +  PUERTO);
              System.out.println("Esperando Clientes…");
              miCliente = miServidor.accept();
              System.out.println("Puerto  utilizado:"   +  miCliente.getPort());
              System.out.println("El Cliente  " + miCliente.getInetAddress().getHostAddress() + "  se conecto…");
              ServidorHilo9 hilo = new ServidorHilo9(miCliente);
              misClientes.add(hilo);
              hilo.start();
          }
        }catch(Exception error){
              System.out.println(error);
        }
    }
    
    public static void main(String args[]){
        new Servidor9();
    }
}

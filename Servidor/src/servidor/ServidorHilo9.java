package servidor;

import java.net.Socket;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.IOException;

public class ServidorHilo9 extends Thread {
             private DataOutputStream enviaDatos;
             private DataInputStream recibeDatos;
             private String username;
             private String puerto="";
             
             public  ServidorHilo9(Socket miCliente){
                try{
                   puerto=""+miCliente.getPort();
                   enviaDatos = new DataOutputStream(miCliente.getOutputStream());
                   recibeDatos = new DataInputStream(miCliente.getInputStream());
                   username = miCliente.getInetAddress().getHostAddress();
                   }catch(IOException error){
                       System.out.println("Error al crear flujos de comunicación");
                   }
             }
             public  String  getUserName(){
                return username;
             }
             private void enviar(String mensaje){
                try{
                enviaDatos.writeUTF(mensaje);
                enviaDatos.flush();
                }catch(IOException error){
                  System.out.println("Error al enviar mensaje");
                }
             }
             private String recibir(){
                String mensaje = null;
                try{
                   mensaje = recibeDatos.readUTF();
                       }catch(IOException error){
                              System.out.println("Error al enviar el mensaje");
                       }
                       return mensaje;
             }
             private String procesaUserNames(){
                String usuarios = "";
                for(int i=0; i<Servidor9.misClientes.size(); i++){
        
                usuarios = usuarios + (i+1) + " -" + Servidor9.misClientes.get(i).getUserName() + ",";
                }
                return usuarios.substring(0, usuarios.length()  - 1);//a los usuarios al final le suma una coma
            }
            public void run(){
                String mensaje = "";
                while(true){
                    mensaje = recibir();
                    if(mensaje.equals("{BYE}")){
                        Servidor9.misClientes.remove(this);
                        break;
                    }else if(mensaje.startsWith("{HELLO}")){
                        String dato[] = mensaje.split(",");
                        username = dato[1];
                        mensaje = username + "\n se conectó!{LIST}" + procesaUserNames();
                    }else{
                        mensaje = " puerto: "+puerto+ " "+ username + " dice:  " + mensaje;
                    }
                    for(int i=0; i<Servidor9.misClientes.size(); i++){ 
                        Servidor9.misClientes.get(i).enviar(mensaje);
                    }
                }
            }


}

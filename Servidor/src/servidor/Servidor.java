package servidor;
import java.net.ServerSocket;
public class Servidor {
    private final int PUERTO=9998;
    private ServerSocket miServidor;
    
    public Servidor(){
        try{ 
            miServidor= new ServerSocket(PUERTO);
            while(true){
                System.out.println("Servidor activo, Puerto: "+PUERTO);
                System.out.println("Esperando cliente");
                miServidor.accept();
                System.out.println("Un cliente se conectó");
            }
        }
        catch (Exception error){
            System.out.println(error);}
      
    }
    public static void main(String[] args) {
              new Servidor();
    }
    
}
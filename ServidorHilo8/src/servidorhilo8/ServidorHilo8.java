/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorhilo8;
import java.net.Socket;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.DataInputStream;
import java.io.IOException;

public class ServidorHilo8 extends Thread {   //Hereda un Thread
    private DataOutputStream enviaDatos;
    private DataInputStream recibeDatos;
    private String puerto="";
    private String username="";
    
    public ServidorHilo8(Socket miCliente){   //RecibeComoParametroElSocketYasiUnProcesoIndependiente 
           try{
               puerto=""+miCliente.getPort();
               username=miCliente.getInetAddress().getHostAddress();
               System.out.println("Desde el ServidorHilo" + username+ " el puerto: "+ puerto + " fue asignado a un hilo" );
               enviaDatos = new DataOutputStream(miCliente.getOutputStream());
               recibeDatos = new DataInputStream(miCliente.getInputStream());
           }catch(IOException error){
               System.out.println("Error al crear flujos de comunicacion");
           }
    
    }
    private void enviar(String mensaje){
           try{
               enviaDatos.writeUTF(mensaje);
               enviaDatos.flush();
           }catch(IOException error){
               System.out.println("Error al enviar mensaje!");
           }
    }
    private String recibir(){
           String mensaje = null;
           try{
               mensaje = recibeDatos.readUTF();
           }catch(IOException error){
               System.out.println("Error al enviar mensaje!");
           }
           return mensaje;
    }
    public void run(){    //EsteProcesoEstaraAcargoDeRecibirYenviarMensajesHastaQueElClienteFinaliceLaComunica
           String mensaje;
           do{
              mensaje = recibir();
              enviar("ECO: "+username+"   puerto:   "+puerto+">" +  mensaje);
            
           }while(!mensaje.equals("BYE"));
            mensaje="Conversación Finalizada";
            enviar("ECO:  "+username+" puerto:   "+puerto+">" + mensaje);    
    }
    
}

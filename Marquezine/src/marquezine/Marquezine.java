/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package marquezine;
import javax.swing.*;
import java.awt.*;

public class Marquezine extends JFrame implements Runnable{
    private JLabel lblEtiqueta1;
    private JLabel lblEtiqueta2;
    
    public Marquezine(String msg1, String msg2){
    setLayout(new BorderLayout());
    lblEtiqueta1 = new JLabel(msg1);
    lblEtiqueta1.setFont(new Font("Papyrus",Font.BOLD,30));
    lblEtiqueta1.setForeground(Color.red);
    lblEtiqueta2 = new JLabel(msg2);
    lblEtiqueta2.setFont(new Font("Comic Snas MS",Font.BOLD,30));
    lblEtiqueta2.setForeground(new Color(200,145,189));
    add(lblEtiqueta1,BorderLayout.NORTH);
    add(lblEtiqueta2,BorderLayout.SOUTH);
    setTitle("Marquezine");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setResizable(false);
    setSize(400,300);
    setLocationRelativeTo(null);
    setVisible(true);
    }
    
    public void run(){
    }
    
    public static void main(String[] args) {
        new Marquezine("1 GUN´S AND ROSES", "AEROSMITH");
        new Marquezine("2 GUN´S AND ROSES", "AEROSMITH");
        //NO SE CREA Y NO SE ENVIA EL HILO
    }
    
}
